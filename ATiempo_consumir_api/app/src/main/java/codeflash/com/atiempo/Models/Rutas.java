package codeflash.com.atiempo.Models;

import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
public class Rutas {
    private String descripcion;
    private Ruta ruta;

    public String getDescripcion() {
        return descripcion;
    }

    public Ruta getRuta() {
        return ruta;
    }
}
