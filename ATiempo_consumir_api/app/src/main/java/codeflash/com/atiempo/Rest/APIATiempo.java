package codeflash.com.atiempo.Rest;

import codeflash.com.atiempo.Models.RutaResults;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Luigi on 8/10/2016.
 */
public interface APIATiempo {

    @GET("ruta")
    Call<RutaResults> getRutas();
}
