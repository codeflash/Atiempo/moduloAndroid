package codeflash.com.atiempo.Models;


import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
public class Ruta {
    private List<Point> points;

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
