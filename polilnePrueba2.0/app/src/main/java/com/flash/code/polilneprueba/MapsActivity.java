package com.flash.code.polilneprueba;

//import android.*;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
// import android.util.Log;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String cadena;
    //private EditText consulta;
    //private ObtenerWebService hiloconexion;
    private aaa hiloconexion;
    private LocationManager locManager;
    int i=0;
    int e=0;
    List<LatLng> positions = new ArrayList<>(); ;
    LatLng a[]=new LatLng[50];
    TextView lattxt;
    TextView lngtxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        cadena = "https://roads.googleapis.com/v1/snapToRoads?path=";
        lattxt = (TextView) findViewById(R.id.lat);
        lngtxt= (TextView) findViewById(R.id.lng);
        //consulta=(EditText) findViewById(R.id.url);

        registerLocations();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng arequipa = new LatLng(-16.428386, -71.521210);

        mMap.addMarker(new MarkerOptions()
                .position(arequipa)
                .title("Jose luis bustamente"));




        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(arequipa, 18));

    }

    private void registerLocations() {

        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 4000, 0,new MyLocationListener());
    }

    public class MyLocationListener  implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            Double valorLatitud=location.getLatitude();
            Double valorLongitud=location.getLongitude();
            positions.add(new LatLng(valorLatitud, valorLongitud));
            //a[i]=new LatLng(valorLatitud,valorLongitud);
            Log.d("codeflash" , positions.size() + "" );
            if(positions.size() > i) {

                mMap.addMarker(new MarkerOptions()
                        .position(positions.get(i))
                        .title("Punto"));

                if (i >= 1) {
                    mMap.addPolyline(new PolylineOptions().add(positions.get(e), positions.get(i))
                            .width(10)
                            .color(Color.RED)
                    );
                }
                if (i == 5) {
                    //hiloconexion = new ObtenerWebService();
                    hiloconexion = new aaa();
                    hiloconexion.execute(positions);
                }
            }
            e=i;
            Log.d("codeflash" , i + "" );
            i++;
            String lat="";
            String lng="";
            lat=String.valueOf(valorLatitud);
            lng=String.valueOf(valorLongitud);
            lattxt.setText(lat);
            lngtxt.setText(lng);

        }



        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    public class aaa extends AsyncTask<List<LatLng> , Integer , String>{

        @Override
        protected String doInBackground(List<LatLng>... params) {
            String key = "AIzaSyDdm9-xc0Y3kh6oKouN6d9_qDWT7RThGgo";
            String devuelve = "";
            i=0;
            for(LatLng lnLA : params[0]){
                if(params[0].size()-1 == i){
                    cadena = cadena.concat(lnLA.latitude + "");
                    cadena = cadena.concat(",");
                    cadena = cadena.concat(lnLA.longitude + "");
                }else{
                    cadena = cadena.concat(lnLA.latitude + "");
                    cadena = cadena.concat(",");
                    cadena = cadena.concat(lnLA.longitude + "");
                    cadena = cadena.concat("|");
                }
                i++;
            }
            cadena = cadena.concat("&key="+key);
            Log.d("codeflash1" , cadena);
            URL url = null; // Url de donde queremos obtener información
            try {
                url = new URL(cadena);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //Abrir la conexión
                connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                        " (Linux; Android 1.5; es-ES) Ejemplo HTTP");
                //connection.setHeader("content-type", "application/json");

                int respuesta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();

                if (respuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(connection.getInputStream());  // preparo la cadena de entrada
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));  // la introduzco en un BufferedReader
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);        // Paso toda la entrada al StringBuilder
                    }
                    JSONObject respuestaJSON = new JSONObject(result.toString());   //Creo un JSONObject a partir del StringBuilder pasado a cadena
/*
                            List<LatLng> list = PolyUtil.decode(result.toString());
                    PolylineOptions options=new PolylineOptions().width(10).color(Color.BLACK);
                    options.addAll(list);
                    mMap.addPolyline(options);
*/
                    JSONArray jsonSnapeds = respuestaJSON.getJSONArray("snappedPoints");
                    LatLng obj1 = new LatLng(-16.427512117883726,-71.5219058722548);
                   // Route objroute = new Route(obj1);
                List <LatLng> ruta = new List<LatLng>() {
                    @Override
                    public int size() {
                        return 0;
                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }

                    @Override
                    public boolean contains(Object o) {
                        return false;
                    }

                    @NonNull
                    @Override
                    public Iterator<LatLng> iterator() {
                        return null;
                    }

                    @NonNull
                    @Override
                    public Object[] toArray() {
                        return new Object[0];
                    }

                    @NonNull
                    @Override
                    public <T> T[] toArray(T[] a) {
                        return null;
                    }

                    @Override
                    public boolean add(LatLng latLng) {
                        return false;
                    }

                    @Override
                    public boolean remove(Object o) {
                        return false;
                    }

                    @Override
                    public boolean containsAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public boolean addAll(Collection<? extends LatLng> c) {
                        return false;
                    }

                    @Override
                    public boolean addAll(int index, Collection<? extends LatLng> c) {
                        return false;
                    }

                    @Override
                    public boolean removeAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public boolean retainAll(Collection<?> c) {
                        return false;
                    }

                    @Override
                    public void clear() {

                    }

                    @Override
                    public LatLng get(int index) {
                        return null;
                    }

                    @Override
                    public LatLng set(int index, LatLng element) {
                        return null;
                    }

                    @Override
                    public void add(int index, LatLng element) {

                    }

                    @Override
                    public LatLng remove(int index) {
                        return null;
                    }

                    @Override
                    public int indexOf(Object o) {
                        return 0;
                    }

                    @Override
                    public int lastIndexOf(Object o) {
                        return 0;
                    }

                    @Override
                    public ListIterator<LatLng> listIterator() {
                        return null;
                    }

                    @NonNull
                    @Override
                    public ListIterator<LatLng> listIterator(int index) {
                        return null;
                    }

                    @NonNull
                    @Override
                    public List<LatLng> subList(int fromIndex, int toIndex) {
                        return null;
                    }
                };
                    for(int i=0;i<= jsonSnapeds.length()-1 ;i++){
                        JSONObject jsonSnaped = jsonSnapeds.getJSONObject(i);
                        JSONObject jsonLocation = jsonSnaped.getJSONObject("location");
                        Log.d("codeflash",jsonLocation.getString("latitude") +" y "+jsonLocation.getString("longitude"));
                        ruta.add(i,new LatLng(jsonLocation.getDouble("latitude"),jsonLocation.getDouble("longitude")));
                                 //  ruta.add(new LatLng(jsonLocation.getDouble("latitude") ,jsonLocation.getDouble("longitude")));
                        mMap.addPolyline(new PolylineOptions().add(ruta.get(i))
                                .width(10)
                                .color(Color.BLACK)
                        );
                    }


                    //public void correccionR(List <LatLng> ruta){

                 //      PolylineOptions options=new PolylineOptions().width(10).color(Color.BLACK);
                 //     options.addAll(ruta);
                 //   mMap.addPolyline(options);
              //}


                  //  correccionR(ruta);
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return devuelve;
        }
    }



        }
